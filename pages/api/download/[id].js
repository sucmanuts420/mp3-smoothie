import ytdl from "ytdl-core";
import ffmpeg from "fluent-ffmpeg";
import fs from "fs";
import path from "path";

export default function handler(req, res) {

  const { id } = req.query;

  const stream = ytdl(`https://www.youtube.com/watch?v=${id}`, {
    quality: "highestaudio",
    filter: "audioonly",
  });

  ffmpeg(stream)
    .toFormat("mp3")
    .audioBitrate(320)
    .save(`./temp/${id}.mp3`)
    .on("end", () => {
      const filePath = path.join(process.cwd(), `/temp/${id}.mp3`);
      const fileBuffer = fs.readFileSync(filePath);
      res.setHeader("Content-Type", "audio/mpeg");
      res.send(fileBuffer);
    });
    
}
